var baseurl = 'https://d3vh4p1.ekita.co:444/scripts/execute.pike?script=886&request='
var logindata = { "username":"", "password":"" }
var ekita = angular.module('eKita', [], function($routeProvider, $locationProvider) 
                {
                    $routeProvider.
                    when('/courses', {templateUrl:'courses.html', controller:courses}).
                    when('/course/:courseid', {templateUrl:'course.html', controller:course}).
                    when('/overview', {templateUrl:'overview.html', controller:overview}).
                    when('/profile', {templateUrl:'profile.html', controller:profile}).
                    when('/settings', {templateUrl:'settings.html', controller:settings}).
                    otherwise({redirectTo:'/overview'})
                })

function sTeam_get(request, handler, http, window)
{
    logexp("sTeam-get", request, logindata.username, pad(logindata.password.length, "*"), window.btoa(logindata.username + ":" + logindata.password))
    http.get(baseurl+request, {headers:{'Authorization':'Basic '+window.btoa(logindata.username + ":" + logindata.password)}}).success(handler)
}

function sTeam_post(request, data, handler, http, window)
{
    http.post(baseurl+request, data, {headers:{'Authorization':'Basic '+window.btoa(logindata.username + ":" + logindata.password)}}).success(handler)
}

function eKitaFrame($scope, $http, $window)
{
    var handle_request = function(data, status)
    {
        logexp("frame", "request:", data)
        $scope.req = data
        $scope.me = data.user
    }
    sTeam_get('/', handle_request, $http, $window)
    // $http.get(baseurl+'/').success(handle_request)
}

function overview($scope, $http)
{
    $scope.username = ""
    $scope.password = ""
    $scope.loginp = function()
                    {
                        return !!(logindata.username && logindata.password)
                    }

    $scope.login = function()
    {
        logexp("login", $scope.username, pad($scope.password.length, "*"))
        logindata.username = $scope.username
        logindata.password = $scope.password
        $scope.username = ""
        $scope.password = ""
    }
}

function showtabs($scope, $http)
{
    $scope.tabs = open_tabs
    $scope.load = function()
    {
        logexp("showtabs", open_tabs)
    }
    $scope.load()
}

function edit(fields, editfield, event)
{
    /* if (typeof(event.target.form) != "undefined" && fields[editfield] ==  true) 
        fields[editfield] = false */
    for (field in fields)
        fields[field] = false
    fields[editfield] = true
    return fields
}

function settings($scope, $http, $window)
{
    load_tab({ "name":"Settings" })
    var handle_request = function(data, status)
    {
        logexp("settings", "request", Object.keys(data), data)
        $scope.user = data.user
        $scope.settings = data.settings
    }
    sTeam_get('settings', handle_request, $http, $window)
    // $http.get(baseurl+'settings').success(handle_request)

    $scope.editp = {}
    $scope.edit = function(field, event)
    {
        $scope.editp = edit($scope.editp, field, event)
    }

    $scope.save = function()
    {
        sTeam_post('settings', $scope.settings, handle_request, $http, $window)
        // $http.post(baseurl+'settings', $scope.settings).success(handle_request)
        $scope.edit('')
    }
}

function profile($scope, $http, $window)
{
    load_tab({ "name":"Profile" })
    var handle_request = function(data, status)
    {
        logexp("profile", "request", Object.keys(data), data)
        $scope.req = data
        $scope.id = data.user.id
        $scope.fullname = data.user.fullname
    }
    sTeam_get('/', handle_request, $http, $window)
    // $http.get(baseurl+'/').success(handle_request)
}

function course($scope, $http, $routeParams, $window)
{
    var handle_request = function(data, status)
    {
        load_tab({ "name":data.name })
        logexp("course request", $routeParams.courseid, Object.keys(data), data)
        $scope.req = data
        $scope.id = data.id
        $scope.description = data.description
        $scope.schedule = data.schedule
        $scope.members = data.members
        $scope.documents = data.documents
    }
    sTeam_get($routeParams.courseid, handle_request, $http, $window)
    // $http.get(baseurl+$routeParams.courseid).success(handle_request)
}

function courses($scope, $http, $window)
{
    load_tab({ "name":"Courses" })

    var handle_courses = function(data, status)
    {
        logexp("courses", "request:", data)
        $scope.courses = data.classes
    }

    $scope.courses = []

    $scope.fetch = function(path)
    {
        if (!path)
            path = "/"
        sTeam_get(path, handle_courses, $http, $window)
        // $http.get(baseurl+path).success(handle_courses)
    }
    $scope.fetch()
}

function load_tab(tab)
{
    if (!contains(open_tabs, tab))
    {
        open_tabs.push(tab)
        logexp("tabs", open_tabs)
    }
}

var open_tabs = []

function contains(a, obj) 
{
    for (var i = 0; i < a.length; i++) 
    {
        if (a[i].name == obj.name)
            return true
    }
    return false
}

function log()
{
    var res = "(ekita";
    for (arg in arguments)
    {
        val = arguments[arg]
        if (val instanceof Array)
            res += " ("+val+")"
        else if ((typeof val === "string" || val instanceof String) && val.search(" ") >= 0 && val.search("\"") == -1)
            res += " \""+val+"\""
        else
            res += " "+val
    }
    res += ")"
    console.log(res)
}

function logexp()
{
    console.log(sexp(["ekita"].concat(Array.prototype.slice.call(arguments))))
}

function sexp(data, indent) 
{
    if (!indent)
        indent = 0

    var children, elem, s
    s = ''
    if (Array.isArray(data)) 
    {
        var children = []
        for (key in data) 
        {
            children.push(sexp(data[key], indent+1))
        }
        if (children.length)
            return "(" + children.join("\n"+pad(indent+1)) + ")"
        else 
            return "()"
    } 
    else if ((typeof data === "string" || data instanceof String) && data.search(" ") == -1 && data.search("\"") == -1 && data != "" && !Number(data) && data != "0")
    {
        /* strings that look like numbers need to be quoted */
        return data
    }
    else if (typeof data === "object" && !data instanceof String && Object.keys(data).length)
    {
        var children = []
        for (key in data)
        {
            var keystr = sexp(key, indent+1)
            var pair = "("+keystr+" . "+sexp(data[key], indent+keystr.length+5)+")"
            children.push(pair)
        }
        if (children.length)
            return "("+children.join("\n"+pad(indent+1))+")"
        else 
            return "()"
    }
    else /* if ((typeof data === "string" || data instanceof String || typeof data === "number" || data instanceof Number)) */
    {
        return JSON.stringify(data)
    }
}

function pad(length, padchar)
{
    if (!padchar)
        padchar = " "
    return new Array(length+1).join(padchar)
}
